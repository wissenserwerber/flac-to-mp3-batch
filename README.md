# flac-to-mp3-converter

## Ein kleines Shellskript das die flac (CD Audio) Dateien in mp3 mittels ffmpeg verwandelt. Die Verwandlung findet in Chargen statt.

### Voraussetzung

Das Werkzeug [ffmpeg](https://ffmpeg.org) soll installiert worden sein.